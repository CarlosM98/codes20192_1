package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static double kmTom1(double km) { double mt=0; mt=km*1000; return mt; }

//Km to metters
    public static double kmTom(double km)
    {
        double mt=0; mt=km*1000; return mt;
    }
    
    //Km to cm
    public static double kmTocm(double km)
    {
        double cm=0; cm=km*1000*100; return cm;
    }

//milimetters to metters
    public static double mmTom(int mm)
    {
        double mm1=0;

        mm1=(double)mm;

        double mt=0; mt=mm1/1000; return mt;
    }
//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        double foot=0; foot=miles*5280; return foot;
    }

//convert yards to inches
    public static int yardToInch(int yard)
    {     int inch=0;
        inch=yard*3*12;
        return inch;
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        double miles=0;
        miles=inch/63360;

        return miles;
    }
//convert foot to yards
    public static int footToYard(int foot)
    {
        int yard;
        yard=foot/3;


        return yard;
    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(String km)
    {
        double inch=0,km2=0;

        km2 = Double.parseDouble(km);


        inch=km2*39370.079;

        return inch;}

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {    double foot=0,mm1;
        mm1 = Double.parseDouble(mm);
        foot=mm1/304.8;

        return foot;}
//convert yards to cm    
    public static double yardToCm(String yard)
    {
        double yard1=0,cm=0;
        yard1 = Double.parseDouble(yard);

        cm=yard1*91.44;
        return cm;}


}
