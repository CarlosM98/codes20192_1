package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric {

//Calculate the area for a square using one side
public static int squareArea(int side)
{int area=0;
    area=side*side;
    return area;
}

//Calculate the area for a square using two sides
public static int squareArea(int sidea, int sideb) 
{    int area=0;
    area=sidea*sideb;
    return area;
}

//Calculate the area of circle with the radius
public static double circleArea(double radius)
{
    double area=0;
    area=radius*radius*(3.1416);
    return area;


}

//Calculate the perimeter of circle with the diameter
public static double circlePerimeter(int diameter)
{
    double perimetro=0;
    perimetro=diameter*(3.1416);
    return perimetro;

}

//Calculate the perimeter of square with a side
public static double squarePerimeter(double side)
{

    double perimetro=0;
    perimetro=side*4;
    return perimetro;


}

//Calculate the volume of the sphere with the radius
public static double sphereVolume(double radius)
{
    double volumen4=0;
    double r=0;

    System.out.println(""+radius+",");
    r=radius*radius*radius;
    System.out.println(""+r+",");
    volumen4=1.3333333*PI*r;

    System.out.println(""+volumen4+",");
    return volumen4;



}

//Calculate the area of regular pentagon with one side
public static float pentagonArea(int side)
{
    return 0;
}

//Calculate the Hypotenuse with two cathetus
public static double calculateHypotenuse(double catA, double catB)
{
    return 0.0;
}

}
